#include <stdio.h>
#include <subRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

static float deg2Fah(subRecord *precord) {

    //precord->val++;
    precord->val = precord->a*1.8 + 32.0 ;

    return 0;
}

/* Note the function must be registered at the end!*/
epicsRegisterFunction(deg2Fah);


