// http://www.smolloy.com/2015/12/serial-control-of-an-arduino/

const int DHTPIN = 8;     // Digital pin connected to the DHT sensor 

const int ledPin = 11;

int sensorval = 0;
float tempval = 0;
char lastRecvd;

int ledState = 0;
String input = ""; // This will capture the serial input commands

// DHT FUNCTION
void read_DHT(){

      uint32_t DHT_thr=50; //threshold for byte reading
      byte DHT_Ti=0; //temp: integral part
      byte DHT_Td=0; //temp: decimal part
      byte DHT_Hi=0; //humidity: integral part
      byte DHT_Hd=0; //humidity: decimal part
      byte DHT_check=0; //checksum

      // Go into high impedence state to let pull-up raise data line level and
      // start the reading process.
      pinMode(DHTPIN, INPUT_PULLUP);
      delay(1);

      // First set data line low for a period according to sensor type
      pinMode(DHTPIN, OUTPUT);
      digitalWrite(DHTPIN, LOW); // Start signal
      delay(20); // data sheet says at least 18ms, 20ms just to be safe

      uint32_t cycles[40];
      {
        // End the start signal by setting data line high for 40 microseconds.
        pinMode(DHTPIN, INPUT_PULLUP);

        // Delay a moment to let sensor pull data line low.
        delayMicroseconds(1);

        // Now start reading the data line to get the value from the DHT sensor.

        // Turn off interrupts temporarily because the next sections
        // are timing critical and we don't want any interruptions.
        noInterrupts();

        // First expect a low signal for ~80 microseconds followed by a high signal
        // for ~80 microseconds again.
        pulseIn(DHTPIN, HIGH);

        for (int i = 0; i < 40; i += 1) {
          cycles[i] = pulseIn(DHTPIN, HIGH);
        };

      }; // Timing critical code is now complete.
      interrupts();

      bool cycles_b[40];
      {
        for (int i = 0; i < 40; i += 1) {
          cycles_b[i] = (cycles[i] > DHT_thr)  ;
        };

      }; // Timing critical code is now complete.

      for (int i = 0; i < 8; i++){
           DHT_Hi += ( cycles_b[i] << (7-i));
      }
      for (int i = 8; i < 16; i++){
           DHT_Hd += ( cycles_b[i] << (15-i));
      }
      for (int i = 16; i < 24; i++){
           DHT_Ti += ( cycles_b[i] << (23-i));
      }
        for (int i = 24; i < 32; i++){
           DHT_Td += ( cycles_b[i] << (31-i));
      }
      for (int i = 32; i < 40; i++){
           DHT_check += ( cycles_b[i] << (39-i));
      }

      if (DHT_check == (DHT_Hi+DHT_Hd+DHT_Ti+DHT_Td)  ){
        Serial.print("H ");
        Serial.print(  DHT_Hi);
        Serial.print(".");
        Serial.print(DHT_Hd);
      
        Serial.print(" ");
      
        Serial.print(DHT_Ti);
        Serial.print(".");
        Serial.print(DHT_Td);
        Serial.print("\n");
      }
      else{
        Serial.print("H -10.0 -10.0");
        Serial.print("\n");
      };//if

};//DHT 

void setup() {
 pinMode(ledPin, OUTPUT);
 pinMode(A0, INPUT);
 pinMode(A1, INPUT);
 Serial.begin(9600); // Make sure the Serial commands match this baud rate

 analogWrite(ledPin, ledState);
 }

void loop () {
 sensorval = analogRead(A0);
 tempval = analogRead(A1);
 while (Serial.available()>0){

    lastRecvd = Serial.read();

    if (lastRecvd == '\n'){

        switch(input[0]){
           case 'R':
              Serial.print("R ");
              Serial.print(sensorval);
              Serial.print("\n");
              input = ""; 
              break;
            case 'W':
               input = input.substring(1,input.length());
               ledState = constrain(input.toInt(), 0, 255);
               analogWrite(ledPin, ledState);
               Serial.println("LED Written");
               input = "";
               break;
            case 'T':
              Serial.print("T ");
              Serial.print(tempval);
              Serial.print("\n");
              
              input = ""; 
              break;
            case 'H':
              read_DHT(); 
              input = ""; 
              break;

            default:
               input = ""; 
               break;}//switch
           
     }else { // Input is still coming in
        input += lastRecvd;}

  }//while
}//loop
