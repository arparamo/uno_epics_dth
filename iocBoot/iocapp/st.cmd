#!../../bin/linux-x86_64/app

#- You may have to change app to something else
#- everywhere it appears in this file

< envPaths
cd "${TOP}"

epicsEnvSet(STREAM_PROTOCOL_PATH,"$(TOP)/appApp/Db")


## Register all support components
dbLoadDatabase "$(TOP)/dbd/app.dbd"
app_registerRecordDeviceDriver pdbbase

# Serial Port
drvAsynSerialPortConfigure("SERIALPORT","/dev/ttyS3",0,0,0)
asynSetOption("SERIALPORT",-1,"baud","9600")
asynSetOption("SERIALPORT",-1,"bits","8")
asynSetOption("SERIALPORT",-1,"parity","none")
asynSetOption("SERIALPORT",-1,"stop","1")
asynSetOption("SERIALPORT",-1,"clocal","Y")
asynSetOption("SERIALPORT",-1,"crtscts","N")

# Modbus Port
#drvModbusAsynConfigure("read_holding", "SERIALPORT", 1, 3, 0, 1, 0, 100, "Holding")

#Load Records

dbLoadRecords("$(TOP)/appApp/Db/uno.db","PORT='SERIALPORT',SS='uno'")
dbLoadRecords("$(TOP)/appApp/Db/DHT.db","PORT='SERIALPORT',SS='uno'")

dbLoadRecords("$(TOP)/appApp/Db/deg2Fah.db", "SS='uno', var='temp'")
dbLoadRecords("$(TOP)/appApp/Db/deg2Fah.db", "SS='uno', var='DHT_T'")


#cd "${TOP}/iocBoot/${IOC}"
cd "${TOP}"

asynSetTraceMask("SERIALPORT", -1, 2)
asynSetTraceIOMask("SERIALPORT", -1, 2)

iocInit

## Start any sequence programs
#seq sncxxx,"user=angel"
