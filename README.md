# Temperature Sensor: Arduino and EPICS

This project includes temperature and humidity sensor implemented in an arduino board and controlled in EPICs.
Also a photosensor and a led are included in the arduino

THe project is based on The EPICs code is based on SMollow tutorial  http://www.smolloy.com/category/arduino/

## Arduino Code

The arduino code is in ´arduino/arduino.ino´

## EPICs code

EPICs code (db and proto files) are included in `appApp/Db`. 
- DTH.db includes the database for the digital temp. and humidity sensor.
- uno.db includes the database for a analogue temp. sensor, a photosensor and a led.

A subroutine for Celsios to Farenheit. conversion is implemented in ´appApp/src/deg2Fah.cpp´




